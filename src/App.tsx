import React from 'react';
import { CarouselPage } from './Carousel';

function App() {
  return (
    <CarouselPage />
  );
}

export default App;
