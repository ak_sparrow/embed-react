import { FC } from "react";
import { Star } from "../icons/Star";

export const Stars: FC<{ rating?: number }> = ({
  rating = 1,
}) => {
  const fillArray = Array.from({ length: 5 });

  return (
    <div className="ss-embed-review__stars">
      {fillArray.map((_, i) => (
        <Star color={i < rating ? "#F5C744" : "#CCCDCE"} key={i} />
      ))}
    </div>
  );
};
